# -*- coding: utf-8 -*-
#  Copyright 2011 Takeshi KOMIYA
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import blockdiag.drawer
from seqdiag.metrics import DiagramMetrics
from blockdiag.utils import XY
from seqdiag import elements

class DiagramDraw(blockdiag.drawer.DiagramDraw):
    def create_metrics(self, *args, **kwargs):
        return DiagramMetrics(*args, **kwargs)

    def _draw_background(self):
        for node in self.nodes:
            node.activities.sort(key=lambda x: x['level'])

        if self.diagram.shadow_style != 'none':
            for node in self.nodes:
                for activity in node.activities:
                    self.node_activity_shadow(node, activity)

        if self.diagram.shadow_style != 'none':
            for edge in self.edges:
                if isinstance(edge, elements.AltBlock):
                    continue
                self.edge_shadow(edge)
        super(DiagramDraw, self)._draw_background()

    def _draw_elements(self, **kwargs):
        for node in self.nodes:
            self.lifelines(node)

        for block in self.diagram.altblocks:
            self.altblock(block)

        super(DiagramDraw, self)._draw_elements(**kwargs)

        for sep in self.diagram.separators:
            self.separator(sep)

        for group in self.diagram.groups:
            self.group_label(group, **kwargs)

    def node_activity_shadow(self, node, activity):
        box = self.metrics.activity_shadow(node, activity)
        if self.diagram.shadow_style == 'solid':
            self.drawer.rectangle(box, fill=self.shadow,
                              html_class="node node_activity node_activity_shadow node__{}".format(node.id))
        else:
            self.drawer.rectangle(box, fill=self.shadow, filter='transp-blur',
                              html_class="node node_activity node_activity_shadow node__{}".format(node.id))

    def node_activity(self, node, activity):
        box = self.metrics.activity_box(node, activity)
        self.drawer.rectangle(box, width=1, outline=self.diagram.linecolor,
                              fill='moccasin',
                              html_class="node node_activity node__{}".format(node.id))

    def lifelines(self, node):
        for line, style in self.metrics.lifeline(node):
            self.drawer.line(line, fill=self.diagram.linecolor, style=style,
                             html_class="node node_lifeline node__{}".format(node.id))
        for activity in node.activities:
            self.node_activity(node, activity)

    def edge_shadow(self, edge):
        m = self.metrics
        dx, dy = m.shadow_offset

        if edge.leftnote and (not edge.multi or edge.multi_head):
            polygon = m.edge(edge).leftnoteshape
            shadow = [XY(pt.x + dx, pt.y + dy) for pt in polygon]
            if self.diagram.shadow_style == 'solid':
                
                self.drawer.polygon(shadow, fill=self.shadow,
                                    outline=self.shadow,
                                    html_class="edge edge_note_shadow src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))
            else:
                self.drawer.polygon(shadow, fill=self.shadow,
                                    outline=self.shadow, filter='transp-blur',
                                    html_class="edge edge_note_shadow src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

        if edge.rightnote:
            polygon = m.edge(edge).rightnoteshape
            shadow = [XY(pt.x + dx, pt.y + dy) for pt in polygon]
            if self.diagram.shadow_style == 'solid':
                self.drawer.polygon(shadow, fill=self.shadow,
                                    outline=self.shadow,
                                    html_class="edge edge_note_shadow src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))
            else:
                self.drawer.polygon(shadow, fill=self.shadow,
                                    outline=self.shadow, filter='transp-blur',
                                    html_class="edge edge_note_shadow src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

    def edge(self, edge):
        # render shaft of edges
        m = self.metrics.edge(edge)
        shaft = m.shaft
        self.drawer.line(shaft, fill=edge.color, style=edge.style,
                         html_class="edge edge_shaft src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

        # render head of edges
        head = m.head
        if edge.async:
            self.drawer.line((head[0], head[1]), fill=edge.color,
                             html_class="edge edge_async src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))
            self.drawer.line((head[1], head[2]), fill=edge.color,
                             html_class="edge edge_async src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))
        else:
            if head:
                self.drawer.polygon(head, outline=edge.color, fill=edge.color,
                                    html_class="edge edge_head src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

        if edge.failed:
            for line in m.failedmark:
                self.drawer.line(line, fill=edge.color,
                                 html_class="edge edge_head edge_head_failedmark src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

        if edge.leftnote and (not edge.multi or edge.multi_head):
            polygon = m.leftnoteshape
            self.drawer.polygon(polygon, fill=edge.notecolor,
                                outline=self.fill,
                                html_class="edge edge_note_polygon src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

            folded = [polygon[1], XY(polygon[1].x, polygon[2].y), polygon[2]]
            self.drawer.line(folded, fill=self.fill,
                             html_class="edge edge_note_line src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

            self.drawer.textarea(m.leftnotebox, edge.leftnote,
                                 self.metrics.font_for(edge),
                                 fill=edge.color, halign='left',
                                 html_class="edge edge_note_label src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

        if edge.rightnote:
            polygon = m.rightnoteshape
            self.drawer.polygon(polygon, fill=edge.notecolor,
                                outline=self.fill,
                                html_class="edge edge_note_polygon src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

            folded = [polygon[1], XY(polygon[1].x, polygon[2].y), polygon[2]]
            self.drawer.line(folded, fill=self.fill,
                             html_class="edge edge_note_line src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

            self.drawer.textarea(m.rightnotebox, edge.rightnote,
                                 self.metrics.font_for(edge),
                                 fill=edge.color, halign='left',
                                 html_class="edge edge_note_label src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

    def edge_label(self, edge):
        m = self.metrics.edge(edge)

        if edge.label:
            if edge.direction in ('right', 'self'):
                halign = 'left'
            else:
                halign = 'right'

            self.drawer.textarea(m.textbox, edge.label,
                                 self.metrics.font_for(edge),
                                 fill=edge.color, halign=halign,
                                 html_class="edge edge_label src__{} dst__{}".format(edge.left_node.id, edge.right_node.id))

    def separator(self, sep):
        m = self.metrics.separator(sep)
        for line in m.lines:
            self.drawer.line(line, fill=self.fill, style=sep.style,
                             html_class="separator separator_line {}".format(sep.type))

        if sep.type == 'delay':
            self.drawer.rectangle(m.labelbox, fill='white', outline='white',
                                  html_class="separator separator_rect {}".format(sep.type))
        elif sep.type == 'divider':
            self.drawer.rectangle(m.labelbox, fill=sep.color,
                                  outline=sep.linecolor,
                                  html_class="separator separator_rect {}".format(sep.type))

        self.drawer.textarea(m.labelbox, sep.label,
                             self.metrics.font_for(sep), fill=sep.textcolor,
                             html_class="separator separator_label {}".format(sep.type))

    def altblock(self, block):
        m = self.metrics.cell(block)
        ids = " ".join("node__{}".format(node.id) for node in block.nodes)
        self.drawer.rectangle(m, outline=block.linecolor, html_class="altblock altblock_rect "+ids)

        box = m.textbox

        line = [XY(box.x1, box.y2),
                XY(box.x2, box.y2),
                XY(box.x2 + self.metrics.cellsize * 2, box.y1)]
        self.drawer.line(line, fill=block.linecolor, html_class="altblock altblock_line "+ids)

        self.drawer.textarea(box, block.label, fill=block.textcolor,
                             font=self.metrics.font_for(block),
                             html_class="altblock altblock_label "+ids)
