# -*- coding: utf-8 -*-
#  Copyright 2011 Takeshi KOMIYA
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from collections import defaultdict

from seqdiag import parser
from seqdiag.elements import (Diagram, DiagramNode, NodeGroup,
                              DiagramEdge, EdgeSeparator, AltBlock)
from blockdiag.utils import unquote, XY
from blockdiag.utils.compat import u


class DiagramTreeBuilder(object):
    def build(self, tree):
        self.diagram = Diagram()
        self.diagram = self.instantiate(self.diagram, None, tree)

        self.update_node_order()
        self.update_edge_order()
        self.update_altblock_ylevel()
        self.diagram.colwidth = len(self.diagram.nodes)
        self.diagram.colheight = len(self.diagram.edges) + 1

        for sep in self.diagram.separators:
            self.diagram.edges.remove(sep)

        if self.diagram.activation != 'none':
            self.create_activities()

        if self.diagram.autonumber:
            self.update_label_numbered()

        return self.diagram

    def update_edge_order(self):
        for node in self.diagram.nodes:
            node.last = -1

        finish_block_after = defaultdict(lambda : []) # edge -> [altblocks]
        for edge in self.diagram.edges:
            update_edge_order_later = None in finish_block_after
            
            if not update_edge_order_later:
                edge.update_order(self.diagram.nodes)

            # if we are at the end of an alt block
            if edge in finish_block_after or None in finish_block_after:
                key = edge if edge in finish_block_after else None
                block = finish_block_after[key]
                
                start_x = min(blk.xy.x for blk in block)
                stop_x  = max(blk.xy.x + blk.colwidth for blk in block)
                
                over_nodes = [node for node in self.diagram.nodes
                              if start_x <= node.xy.x < stop_x]
                
                max_order = max(blk.stop_order for blk in block)
                
                for node in over_nodes: node.last = max_order
                del finish_block_after[key]
                
            if update_edge_order_later:
                edge.update_order(self.diagram.nodes)
            
            if isinstance(edge, AltBlock) and not edge.type == "task":
                # list all edges and order them in INPUT order (self.diagram.edges) 
                sub_edges = [e for e in self.diagram.edges if e in edge.all_edges]
                
                finish_block_after[sub_edges[-1] if sub_edges else None].append(edge)
                
        height = max([node.last for node in self.diagram.nodes]) + 1
        for group in self.diagram.groups:
            # don't know why we need this +1 ...
            group.colheight = height +1

    def update_altblock_ylevel(self):
        altblocks = self.diagram.altblocks

        for i in range(len(self.diagram.edges) + 1):
            # tasks are special because they can be on the same line
            # ... but are they handled correctly ... ?
            # all the nodes starting at level i
            blocks = [b for b in altblocks 
                         if b.order == i
                         if not b.type == "task"]

            for j, altblock in enumerate(reversed(blocks)):
                altblock.ylevel_top = j + 1

            # all the nodes stopping at level i
            blocks = [b for b in altblocks 
                         if b.stop_order == i
                         if not b.type == "task"]

            for j, altblock in enumerate(reversed(blocks)):
                altblock.ylevel_bottom = j + 1

    def update_label_numbered(self):
        for i, edge in enumerate(self.diagram.edges):
            edge.label = u("%d. %s") % (i + 1, edge.label or "")

    def create_activities(self):
        if not self.diagram.edges:
            return
        
        active_nodes = defaultdict(lambda : 0)

        for i in range(self.diagram.line_number):
            for edge in self.diagram.edges:
                if isinstance(edge, AltBlock): continue
                if edge.order != i: continue

                if edge.activate is False:
                    pass
                elif edge.node1 == edge.node2:
                    if edge.dir.startswith('forward'):
                        active_nodes[edge.node1] += 1
                    elif edge.dir.startswith('back'):
                        active_nodes[edge.node1] -= 1
                elif edge.dir.startswith('forward'):
                    active_nodes[edge.node1] -= 1
                    active_nodes[edge.node2] += 1
                elif edge.dir.startswith('back'):
                    active_nodes[edge.node2] -= 1
                    active_nodes[edge.node1] += 1

            for alt in self.diagram.altblocks:
                if alt.type != "parallel": continue

                top, bottom = alt.top_bottom

                if i == top: inc = 1
                elif i == bottom: inc = -1
                else: continue
                
                for node in alt.all_nodes:
                    if i == top:
                        node.parallel_activity[i] = active_nodes[node]
                    active_nodes[node] += inc

            for node in active_nodes:
                for index in range(active_nodes[node]):
                    node.activate(i, index)

        for node in self.diagram.nodes:
            node.deactivate()
        
    def update_node_order(self):
        x = 0
        uniq = []

        for node in self.diagram.nodes:
            if node not in uniq:
                node.xy = XY(x, 0)
                uniq.append(node)
                x += 1

                if node.group:
                    for subnode in node.group.nodes:
                        if subnode not in uniq:
                            subnode.xy = XY(x, 0)
                            uniq.append(subnode)
                            x += 1

        for group in self.diagram.groups:
            x = min(node.xy.x for node in group.nodes)
            group.xy = XY(x, 0)
            group.colwidth = len(group.nodes)

    def append_node(self, node, group):
        if node not in self.diagram.nodes:
            self.diagram.nodes.append(node)

        if isinstance(group, NodeGroup) and node not in group.nodes:
            if node.group:
                msg = "DiagramNode could not belong to two groups"
                raise RuntimeError(msg)
            
            group.nodes.append(node)
            node.group = group

    def instantiate(self, group, block, tree):
        for stmt in tree.stmts:
            if isinstance(stmt, parser.Node):
                node = DiagramNode.get(stmt.id)
                node.set_attributes(stmt.attrs)
                self.append_node(node, group)
                
                if block is not None:
                    block.nodes.add(node)
                
            elif isinstance(stmt, parser.Edge):
                self.instantiate_edge(group, block, stmt)

            elif isinstance(stmt, parser.Group):
                node = NodeGroup.get(None)
                self.instantiate(node, block, stmt)
                self.diagram.groups.append(node)

            elif isinstance(stmt, parser.Attr):
                if block:
                    block.set_attribute(stmt)
                else:
                    group.set_attribute(stmt)

            elif isinstance(stmt, parser.Separator):
                sep = EdgeSeparator(stmt.type, unquote(stmt.value), block)
                sep.group = group
                self.diagram.separators.append(sep)
                group.edges.append(sep)
                if block is not None:
                    block.separators.append(sep)
                
            elif isinstance(stmt, parser.Fragment):
                subblock = AltBlock(stmt.type, stmt.id)
                if block:
                    subblock.xlevel = block.xlevel + 1
                    block.subblocks.append(subblock)
                self.diagram.altblocks.append(subblock)
                
                self.diagram.edges.append(subblock)
                
                self.instantiate(group, subblock, stmt)
                if block:
                    for edge in subblock.edges:
                        block.edges.append(edge)

            elif isinstance(stmt, parser.Extension):
                if stmt.type == 'class':
                    name = unquote(stmt.name)
                    Diagram.classes[name] = stmt
                elif stmt.type == 'plugin':
                    self.diagram.set_plugin(stmt.name, stmt.attrs)

        return group

    def instantiate_edge(self, group, block, stmt, following=None):
        from_node = DiagramNode.get(stmt.from_node)
        self.append_node(from_node, group)

        to_node = DiagramNode.get(stmt.to_node)
        self.append_node(to_node, group)

        edge = DiagramEdge(from_node, to_node)
        edge.set_dir(stmt.edge_type)
        edge.set_attributes(stmt.attrs)

        edge.multi = following or stmt.followers
        edge.multi_head = not following and stmt.followers
        edge.multi_tail = following and not stmt.followers
            
        if edge.dir in ('forward', 'forwardNarrow', 'both'):
            forward = edge.duplicate()
            forward.dir = 'forward'
            group.edges.append(forward)
            if block:
                block.edges.append(forward)

        if stmt.followers:
            followers = list(stmt.followers)
            next_edge_type, next_to_node = followers.pop(0)
            nested = parser.Edge(stmt.to_node, next_edge_type, next_to_node,
                                 followers, stmt.attrs, stmt.edge_block)
            self.instantiate_edge(group, block, nested, following=edge)
        elif stmt.edge_block:
            self.instantiate(group, block, stmt.edge_block)

        if edge.dir in ('back', 'both'):# and edge.node1 != edge.node2:
            reverse = edge.duplicate()
            reverse.dir = 'back'
            if edge.dir == 'both':
                reverse.style = 'dashed'
                reverse.label = edge.return_label
                reverse.leftnote = None
                reverse.rightnote = None

            group.edges.append(reverse)
            if block:
                block.edges.append(reverse)


class ScreenNodeBuilder(object):
    @classmethod
    def build(cls, tree):
        DiagramNode.clear()
        DiagramEdge.clear()
        NodeGroup.clear()

        return DiagramTreeBuilder().build(tree)
